name := "symmetricspeller"

version := "0.1-SNAPSHOT"
scalaVersion := "2.12.11"

test in assembly := {}
mainClass in assembly := Some("symspeller.App")
assemblyJarName in assembly := "symspeller.jar"
onChangedBuildSource := ReloadOnSourceChanges
credentials += Credentials(Path.userHome / ".sbt" / ".credentials")

scalacOptions := List(
  "-encoding",
  "utf8",
  "-feature",
  "-unchecked",
  "-language:higherKinds",
  "-language:postfixOps",
  "-deprecation",
  "-target:jvm-1.8",
  "-language:_"
)

val v = new {
  val http4s = "0.21.6"
  val newtype = "0.4.3"
  val pureconfig = "0.12.3"
  val pureconfigCats = "0.13.0"
  val serpent = "1.30"
  val pyrolite = "4.4"
  val univocityParser = "2.8.4"
  //  val circle = "0.10.1"
  val circle = "0.13.0"
  val slf4j = "1.7.26"
  val log4cats = "1.1.1"
  val redis = "0.28.0"
}

libraryDependencies ++= Seq(
  "org.http4s" %% "http4s-dsl" % v.http4s,
  "org.http4s" %% "http4s-blaze-server" % v.http4s,
  "org.http4s" %% "http4s-blaze-client" % v.http4s,
  "org.http4s" %% "http4s-circe" % v.http4s,
  "org.http4s" %% "http4s-dsl" % v.http4s,
  "com.github.pureconfig" %% "pureconfig" % v.pureconfig,
  "com.github.pureconfig" %% "pureconfig-cats-effect" % v.pureconfigCats,
  "net.razorvine" % "serpent" % v.serpent,
  "io.estatico" %% "newtype" % v.newtype,
  "io.circe" %% "circe-core" % v.circle,
  "io.circe" %% "circe-literal" % v.circle,
  "net.razorvine" % "pyrolite" % v.pyrolite,
  "com.univocity" % "univocity-parsers" % v.univocityParser,
  "io.circe" %% "circe-parser" % v.circle,
  "io.github.mightguy" % "symspell-lib" % "6.6.129",
  //  "com.github.fomkin" %% "petrovich-scala" % "0.1.1"
  "com.github.cb372" %% "scalacache-redis" % v.redis,
  "com.github.cb372" %% "scalacache-cats-effect" % v.redis,
  "org.http4s" %% "http4s-core" % "0.20.0-M6",
  "io.chrisdavenport" %% "log4cats-core"  % v.log4cats,
  "org.slf4j" % "slf4j-api" % v.slf4j,
  "org.slf4j" % "slf4j-log4j12" % v.slf4j,
  "io.chrisdavenport" %% "log4cats-slf4j"   % v.log4cats
)