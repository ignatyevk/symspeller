package symspeller

import cats.effect.Resource.{fromAutoCloseable, liftF}
import cats.effect.{ConcurrentEffect, ContextShift, IO, Resource, Sync}
import io.chrisdavenport.log4cats.SelfAwareStructuredLogger
import io.chrisdavenport.log4cats.slf4j.Slf4jLogger
import io.github.mightguy.spellcheck.symspell.impl.SymSpellCheck
import org.apache.commons.pool2.impl.GenericObjectPoolConfig
import redis.clients.jedis.JedisPool
import redis.clients.jedis.Protocol.DEFAULT_TIMEOUT
import scalacache.redis.RedisCache
import scalacache.serialization.binary._
import scalacache.{Cache, Mode}
import symspeller.config.{ApplicationConfig, Constant, HttpConfig}
import symspeller.interpreter.{ExternalChecker, Speller, SymSpeller}
import symspeller.utils.Readers.getReader
import symspeller.utils.Utility._

class AppF[F[_] : Sync](implicit F: ConcurrentEffect[IO],cs:ContextShift[IO]) {

  case class Resources(
                        redis: RedisCache[String],
                        conf: ApplicationConfig,
                        terms: Set[String],
                        externalChecker: SymSpellCheck)

  implicit def unsafeLogger: SelfAwareStructuredLogger[IO] = Slf4jLogger.getLoggerFromName[IO]("IO logger")

  implicit val mode: Mode[IO] = scalacache.CatsEffect.modes.async

  def resources: Resource[IO, Resources] =
    for {
      appConfig<-liftF(ApplicationConfig.load)
      verbs <- fromAutoCloseable(getReader(appConfig.paths.verbs)) map readCsv
      nouns <- fromAutoCloseable(getReader(appConfig.paths.nouns)) map readCsv
      averageFrequency = averageFrequencyUnigram(verbs ++ nouns)
      terms <- fromAutoCloseable(getReader(appConfig.paths.terms)) map (readTerms(_, averageFrequency))
      termsForSegmentor <- fromAutoCloseable(getReader(appConfig.paths.terms)) map (readTermsForSegmentor(_, averageFrequency))
      extra <- fromAutoCloseable(getReader(appConfig.paths.extra)) map (readWithAverageFreq(_, averageFrequency))
      cities <- fromAutoCloseable(getReader(appConfig.paths.cities, "windows-1251")) map (readCities(_, averageFrequency))
      externalChecker = ExternalChecker(appConfig.constant).init(verbs ++ nouns ++ extra ++ cities ++ termsForSegmentor)
    } yield Resources(
      RedisCache(
        new JedisPool(
          new GenericObjectPoolConfig(),
          appConfig.http.redisHost,
          appConfig.http.redisPort,
          DEFAULT_TIMEOUT
//          appConfig.secure.redisPassword
        )
      ),
      appConfig,
      terms.keySet,
      externalChecker
    )

  def launch(r: Resources): IO[Unit] = {
    implicit val appConf: ApplicationConfig = r.conf
    implicit val constConf: Constant = r.conf.constant
    implicit val httpConf: HttpConfig = r.conf.http
    new EndpointWiring(
      Speller(
        SymSpeller(
          r.redis,
          r.externalChecker,
          r.terms
        )
      )
    ).launchAll
  }

  def run: Resource[IO, Unit] = resources.flatMap(r =>
    liftF(launch(r))
  )

  private def prepareDict(dict: IO[Map[String, String]], path: String): Unit = {
    dictToString(dict.unsafeRunSync(), path)
  }
}