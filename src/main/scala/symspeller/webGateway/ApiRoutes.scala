package symspeller.webGateway

import cats.data.{Kleisli, OptionT}
import cats.effect.{ContextShift, Effect, IO, Sync}
import cats.syntax.all._
import io.chrisdavenport.log4cats.slf4j.Slf4jLogger
import io.chrisdavenport.log4cats.{Logger, SelfAwareStructuredLogger}
import io.circe._
import io.circe.literal._
import io.circe.syntax._
import org.apache.http.conn.HttpHostConnectException
import org.http4s._
import org.http4s.dsl.Http4sDsl
import org.http4s.headers.Authorization
import org.http4s.server.AuthMiddleware
import symspeller.config.ApplicationConfig
import symspeller.domain.User
import symspeller.interpreter.Speller

class ApiRoutes[F[_] : Effect](speller: Speller[F])(implicit F: Sync[F], conf: ApplicationConfig, log: Logger[IO], sc: ContextShift[IO]) extends Http4sDsl[F] {
  implicit def unsafeLogger: SelfAwareStructuredLogger[F] = Slf4jLogger.getLoggerFromName[F]("Api logger")

  import ApiRoutes._

  val transform: AuthedRoutes[User, F] = AuthedRoutes.of[User, F] {
    case request@POST -> Root / "transform" as _ =>
      for {
        text <- request.req.as[String]
        start = {
          Logger[IO].info("correcting text... \n" + text).unsafeRunSync()
          System.currentTimeMillis()
        }
        tryToCorrect <- speller.transform(text).attempt.to[F]
        finish = System.currentTimeMillis() - start
        response <- tryToCorrect match {
          case Right(corrected) =>
            val converted = json(text, corrected.text, corrected.changes,  corrected.abbreviations)
            Logger[IO].info(converted.toString()).unsafeRunSync()
            Ok(converted.toString())
          case Left(err: HttpHostConnectException) => ServiceUnavailable(err.getMessage + "\n" + err.getCause.getMessage)
          case Left(err: Throwable) =>
            Logger[IO].info(err.getMessage + "\n" + err.getCause.getMessage).unsafeRunSync()
            BadRequest(err.getMessage + "\n" + err.getCause.getMessage)
        }
      } yield {
        Logger[IO].info("Finish correcting, time: " + finish + " ms").unsafeRunSync()
        response
      }
    case x: AuthedRequest[F, User] => badRequestBehavior(x.req)
  }

  val probe: HttpRoutes[F] = HttpRoutes.of[F] {
    case request@GET -> Root / "health" => probeBehavior(request)
    case request@GET -> Root / "ready" => probeBehavior(request)
    case x => badRequestBehavior(x)
  }

  private def probeBehavior(request: Request[F]): F[Response[F]] = {
    val resp = Ok(probeResponse.toString)
    Logger[IO].info(
      s"""
         |${request.toString}
         |${resp.toString}""".stripMargin).unsafeRunSync()
    resp
  }

  private def badRequestBehavior(x: Request[F]) = {
    Logger[IO].info("404 Bad request " + x.method + " " + x.uri + " " + x.headers).unsafeRunSync()
    NotFound()
  }

  val onAuthFailure: AuthedRoutes[String, F] = AuthedRoutes[String, F](req => OptionT.liftF(Forbidden(req.context)))

  val middleware: AuthMiddleware[F, User] = AuthMiddleware.apply[F, String, User](authenticate, onAuthFailure)

  val transformRoutes: HttpRoutes[F] = middleware(transform)
  val probeRoutes: HttpRoutes[F] = probe
}

object ApiRoutes {
  def authenticate[F[_]](implicit F: Sync[F], conf: ApplicationConfig, ev: Logger[F]): Kleisli[F, Request[F], Either[String, User]] = {
    Kleisli(request =>
      request.headers.get(Authorization) match {
        case Some(Authorization(BasicCredentials(user, client_pass))) =>
          if (User(user, client_pass) == defaultUser(conf)) {
            Logger[F].info(s"Authorized user: ${User(user, client_pass).toString}") *>
              F.pure(Right(User(user, client_pass)))
          } else {
            Logger[F].info(s"Failed to authorized: ${User(user, client_pass).toString}") *>
              F.pure(Left("Wrong credentials"))
          }
        case badUser =>
          Logger[F].info(s"Failed to authorized: ${badUser.getOrElse("no header").toString}") *>
            F.pure(Left("Unable to find authorization"))
      }
    )
  }

  private def defaultUser(conf: ApplicationConfig) = User(conf.secure.login, conf.secure.password)

  def json(source: String, result: List[String], changes: Map[String, String], abbrs: Set[String]): Json = {
    json"""{
            "source": $source,
            "result": ${result.mkString(" ")},
            "changes": ${changes.asJson},
            "abbreviations": ${abbrs.asJson}
           }"""
  }

  def probeResponse: Json = {
    json"""{"status": "UP"}"""
  }
}