package symspeller.interpreter

import java.lang.Math.min

import cats.effect.{ContextShift, IO}
import cats.implicits._
import io.chrisdavenport.log4cats.Logger
import io.github.mightguy.spellcheck.symspell.impl.SymSpellCheck
import scalacache.Mode
import scalacache.redis.RedisCache
import symspeller.config.ApplicationConfig
import symspeller.interpreter.SpellingErrors.generateDeletions
import symspeller.utils.Utility.{distance, hget, hset}

case class SymSpeller(redis: RedisCache[String],
                      externalChecker: SymSpellCheck,
                      terms: Set[String],
                     )(implicit conf: ApplicationConfig, mode: Mode[IO], sc: ContextShift[IO]) {
  def doCorrect(token: String)(implicit l: Logger[IO]): IO[Option[String]] = getBestCorrection(token)

  def doSegment(token: String): String = {
    val suggestionItems = externalChecker.wordBreakSegmentation(token, conf.constant.maxTokenLength, conf.constant.maxEditDistanceSegment)
    if (
      suggestionItems.getDistanceSum <= 1 &&
        suggestionItems.getLogProbSum >= -31.9 &&
        //если разбилось то хочется что бы кол-во слов с длинной меньше 3 было не больше 2
        suggestionItems.getCorrectedString.split(" ").count(_.length < 3) < 3)
      suggestionItems.getCorrectedString
    else token
  }

  private def getBestCorrection(token: String)(implicit l: Logger[IO]): IO[Option[String]] =
    (for {
      possibleCorrections <- getPossibleCorrections(token, distance(token))
    } yield {
      possibleCorrections.foldLeft[IO[(Int, Int, Option[String])]](IO(Int.MaxValue, -1, None))((bestCorrection, nextVariant) => bestCorrection.flatMap(best => {
        editDist(token, nextVariant) match {
          case x if x == best._1 =>
            hget("valid", nextVariant)(mode, redis).map {
              case Some(nextFreq) => if (nextFreq.toInt > best._2) (x, nextFreq.toInt, Some(nextVariant)) else best
              case None => Logger[IO].info("thr") *> hset("valid", nextVariant, "1")(mode, redis)
                best
            }
          case y if y < best._1 => hget("valid", nextVariant)(mode, redis).map {
            case Some(nextFreq) => (y, nextFreq.toInt, Some(nextVariant))
            case None => Logger[IO].info("thr") *> hset("valid", nextVariant, "1")(mode, redis)
              best
          }
          case z if z > best._1 => bestCorrection
        }
      }))
    }).flatten.map(x => x._3)

  private def getPossibleCorrections(token: String, distance: Int)(implicit l: Logger[IO]): IO[Set[String]] = {
    generateDeletions(token, distance).map(del => {
      Logger[IO].info("best " + del) *> hget("dict", del)(mode, redis)
    }).parUnorderedSequence.map(_.flatten)
  }

  private def editDist[A](a: Iterable[A], b: Iterable[A]): Int =
    (a foldLeft (0 to b.size).toList) (
      (prev, x) =>
        (prev zip prev.tail zip b).scanLeft(prev.head + 1) {
          case (h, ((d, v), y)) =>
            min(min(h + 1, v + 1), d + (if (x == y) 0 else 1))
        }
    ) last
}