package symspeller.interpreter

import cats.Eval.later
import cats.effect.IO
import symspeller.utils.Utility.distance

import scala.collection.mutable.Map.empty

object SpellingDictionary {
  def constructDictionary(unigramFreq: Map[String, Int]): IO[Map[String, String]] = {
    println("Start prepare dictionary (unigrams + cities + names). words count: " + unigramFreq.size)
    val start = System.currentTimeMillis()
    construct_(unigramFreq).
      guarantee(
        IO(
          println("dictionary created, time to create: " + (System.currentTimeMillis() - start))
        )
      )
  }

  private def construct_(unigramFreq: Map[String, Int]): IO[Map[String, String]] = {
    IO.eval(
      later(
        unigramFreq.keys.filterNot(_.contains('-')).foldLeft(empty[String, String])(
          (map, token) => {
            SpellingErrors
              .generateDeletions(token, distance(token))
              .foreach(deletions => {
                if (!map.contains(deletions) ||
                  map(deletions).length == token.length && unigramFreq(map(deletions)) < unigramFreq(token) ||
                  map(deletions).length > token.length // todo this is best way to improve quality
                ) {
                  map.put(deletions, token)
                }
              })
            map
          }
        ).toMap
      )
    )
  }
}