package symspeller.interpreter

import scala.collection.mutable

object SpellingErrors {
  private val maxTokenLength = 40

  def generateDeletions(token: String, distance: Int): Set[String] = {

    def accumulateDeletions(token: String, distance: Int, accumulator: mutable.Set[String]): Set[String] = {
      for (pos <- token.indices) {
        val deletion = token.patch(pos, Nil, 1)
        if (accumulator.add(deletion) && distance > 1) {
          accumulateDeletions(deletion, distance - 1, accumulator)
        }
      }
      accumulator += token
      accumulator.toSet
    }

    if (token.length < maxTokenLength) accumulateDeletions(token, distance, new mutable.HashSet[String]()) else Set()
  }
}