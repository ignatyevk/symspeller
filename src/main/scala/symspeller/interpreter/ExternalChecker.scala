package symspeller.interpreter

import io.github.mightguy.spellcheck.symspell.common._
import io.github.mightguy.spellcheck.symspell.impl.{InMemoryDataHolder, SymSpellCheck}
import symspeller.config.Constant
import symspeller.utils.QwertyDistanceRussian

case class ExternalChecker(conf: Constant) {

  private val settings: SpellCheckSettings = SpellCheckSettings.builder()
    .countThreshold(conf.countThreshold) // The minimum frequency count for dictionary words to be considered correct spellings.
    .deletionWeight(1.20f) //1.20 ~ 1.40
    .insertionWeight(1.01f) //1.01
    .replaceWeight(1.1f) //0.9f ~ 1.20
    .transpositionWeight(0.87f) //0.7f ~ 1.05
    .maxEditDistance(conf.maxEditDistanceSegment)
    .topK(5) // limit suggestion list to topK entries
    .prefixLength(15)
    .editFactor(0.0) // Edit Factor to compute max edit distance possible for a word.
    .verbosity(Verbosity.ALL).build()

  private val dataHolder = new InMemoryDataHolder(settings, new Murmur3HashFunction())

  private val weightedDamerauLevenshteinDistance = new WeightedDamerauLevenshteinDistance(
    settings.getDeletionWeight(),
    settings.getInsertionWeight(),
    settings.getReplaceWeight(),
    settings.getTranspositionWeight(),
    new QwertyDistanceRussian)

  def init(unigramsFreq: Map[String, String]): SymSpellCheck = {
    unigramsFreq.foreach(x => dataHolder.addItem(new DictionaryItem(x._1, x._2.toDouble, -1.0)))
    new SymSpellCheck(dataHolder, weightedDamerauLevenshteinDistance, settings)
  }
}
