package symspeller.interpreter

import cats.effect.{IO, Sync}
import io.chrisdavenport.log4cats.slf4j.Slf4jLogger
import io.chrisdavenport.log4cats.{Logger, SelfAwareStructuredLogger}
import scalacache.Mode
import symspeller.config.Constant
import symspeller.utils.Utility.{hget, smember}

case class Solution(text: List[String] = List(), changes: Map[String, String] = Map(),  abbreviations: Set[String] = Set())

case class Speller[F[_]](symspeller: SymSpeller)(implicit conf: Constant, mode: Mode[IO], log: Logger[IO]) {
  val maybeAbbr: String => IO[Boolean] = (token: String) => smember("abbr", token)(mode, symspeller.redis)
  val maybeName: String => IO[Option[String]] = (token: String) => hget("name", token)(mode, symspeller.redis)
  val maybeValid: String => IO[Option[String]] = (token: String) => hget("valid", token)(mode, symspeller.redis)
  val unsafeLogger: SelfAwareStructuredLogger[IO] = Slf4jLogger.getLoggerFromName("Speller logger")

  def transform(input: String)(implicit F: Sync[F]): IO[Solution] = {
    val tokens = input.split(" ").map(_.trim)
    val result = tokens.foldLeft(IO(Solution())) { (accumulatorIO, token) =>
      val lowerCaseToken = token.toLowerCase()
      maybeAbbr(lowerCaseToken) flatMap {
        case true => accumulatorIO.map(solve => Solution(solve.text :+ token.map(_.toUpper), solve.changes,solve.abbreviations + token.map(_.toUpper)))
        case false => isCorrected(lowerCaseToken, maybeValid).
          flatMap(isCorrect =>
            if (!isCorrect)
              symspeller.doCorrect(lowerCaseToken).flatMap {
                case Some(corrected) => IO(corrected)
                case None => IO(symspeller.doSegment(lowerCaseToken))
              } flatMap (afterCorrecting => {
                for {
                  capitalized <- capitalize(afterCorrecting, isCorrect)
                  accumulator <- accumulatorIO
                } yield Solution(
                  accumulator.text :+ capitalized,
                  accumulator.changes ++ checkDiff(token, afterCorrecting),
                  accumulator.abbreviations
                )
              })
            else
              for {
                capitalized <- capitalize(lowerCaseToken, isCorrect)
                accumulator <- accumulatorIO
              } yield Solution(
                accumulator.text :+ capitalized,
                accumulator.changes,
                accumulator.abbreviations
              )
          )
      }
    }

    result.map(x => Solution(x.text, x.changes, x.abbreviations))
  }

  private def checkDiff(s1: String, s2: String): Map[String, String] = if (s1 != s2) Map(s1 -> s2) else Map()

  private def capitalize(token: String, isCorrect: Boolean): IO[String] = {
    for {
      name <- maybeName(token)
    } yield {
      if (name.isEmpty) token else if (!isCorrect) token.capitalize else token
    }
  }

  private def isCorrected(token: String, maybeValid: String => IO[Option[String]])(implicit F: Sync[F]): IO[Boolean] = {
    IO(symspeller.terms.contains(token)) flatMap (x => if (x) maybeValid(token).map(_.nonEmpty) else IO(false))
  }

  private def isValidForCorrection(token: String) = token.nonEmpty && token.length > conf.minTokenLength && token.length < conf.maxTokenLength
}