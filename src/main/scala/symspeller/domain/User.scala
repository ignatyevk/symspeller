package symspeller.domain

case class User(login: String, pass: String){
  override def toString = s"login: $login, password: $pass"
}

