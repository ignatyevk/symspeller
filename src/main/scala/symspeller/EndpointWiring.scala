package symspeller

import java.util.concurrent.Executors.{newCachedThreadPool, newFixedThreadPool}

import cats.data.Kleisli
import cats.effect.{ConcurrentEffect, ContextShift, IO, Timer}
import fs2.Stream
import io.chrisdavenport.log4cats.Logger
import org.http4s.server.Router
import org.http4s.server.blaze.BlazeServerBuilder
import org.http4s.syntax.all._
import org.http4s.{Request, Response}
import symspeller.config.ApplicationConfig
import symspeller.interpreter.Speller
import symspeller.webGateway.ApiRoutes

import scala.concurrent.ExecutionContext.fromExecutor
import scala.concurrent.ExecutionContextExecutor


final class EndpointWiring[F[_] : ConcurrentEffect](service: Speller[IO])(implicit
                                                                      conf: ApplicationConfig,
                                                                      ce: ConcurrentEffect[IO],
                                                                      log: Logger[IO],sc:ContextShift[IO]) {
  implicit val httpContext: ExecutionContextExecutor = fromExecutor(newCachedThreadPool())
  implicit val probeContext: ExecutionContextExecutor = fromExecutor(newCachedThreadPool())
  implicit val httpTimer: Timer[IO] = IO.timer(httpContext)
  implicit val probeTimer: Timer[IO] = IO.timer(probeContext)

  val apiService = new ApiRoutes[IO](service)

  val httpApp: Kleisli[IO, Request[IO], Response[IO]] = Router(
    "/" -> apiService.transformRoutes
  ).orNotFound

  val probeApp: Kleisli[IO, Request[IO], Response[IO]] = Router(
    "/actuator" -> apiService.probeRoutes
  ).orNotFound

  def launchHttpService: BlazeServerBuilder[IO] = {
    BlazeServerBuilder.apply[IO](httpContext)(ce, httpTimer)
      .bindHttp(conf.http.port, conf.http.host)
      .withHttpApp(httpApp)
  }

  def launchProbeService: BlazeServerBuilder[IO] = {
    BlazeServerBuilder.apply[IO](probeContext)(ce, probeTimer)
      .bindHttp(conf.http.probePort, conf.http.probeHost)
      .withHttpApp(probeApp)
  }

  def launchAll: IO[Unit] = {
    Stream(launchHttpService.serve, launchProbeService.serve).parJoinUnbounded.compile.drain
  }
}