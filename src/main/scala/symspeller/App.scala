package symspeller

import cats.effect.{ExitCode, IO, IOApp}

object App extends IOApp {
  val app = new AppF[IO]

  override def run(args: List[String]): IO[ExitCode] = app.run.use(_ => IO.never).as(ExitCode.Success)
}