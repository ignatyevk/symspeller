package symspeller.config

import cats.effect.{Blocker, ContextShift, IO}
import pureconfig.ConfigSource
import pureconfig.module.catseffect.syntax.CatsEffectConfigSource

case class HttpConfig(host: String, port: Int,probeHost:String,probePort:Int,redisHost:String,redisPort:Int)

case class DataPath(cities: String,
                     names: String,
                     nouns:String,
                     verbs:String,
                     extra:String,
                     terms:String)

case class Constant(minTokenLength: Int, maxTokenLength: Int,countThreshold:Int,maxEditDistanceSegment:Int)

case class Secure(login:String, password:String, redisPassword:String)

case class ApplicationConfig(http: HttpConfig, paths: DataPath, constant: Constant,secure:Secure)

object ApplicationConfig {
  // does not remove this import!
  import pureconfig.generic.auto._

  def load(implicit cs: ContextShift[IO]): IO[ApplicationConfig] =
    Blocker[IO].use(blocker => ConfigSource.default.at("speller").loadF[IO, ApplicationConfig](blocker))
}