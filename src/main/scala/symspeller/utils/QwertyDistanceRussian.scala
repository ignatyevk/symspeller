package symspeller.utils

import io.github.mightguy.spellcheck.symspell.api.CharDistance
import symspeller.utils.QwertyDistanceRussian.{coordinates, isDiagonal, isDirect}

class QwertyDistanceRussian extends CharDistance {
  private val diagonalConnect = 0.4000000059604645D
  private val directConnect = 0.10000000149011612D
  private val defaultValue = 1.0D

  override def distance(a: Char, b: Char): Double =
    if (isDiagonal(coordinates(a), coordinates(b))) diagonalConnect
    else if (isDirect(coordinates(a), coordinates(b))) directConnect
    else defaultValue

}

object QwertyDistanceRussian {
  private def isDiagonal(a: (Int, Int), b: (Int, Int)): Boolean =
    a._1 - 1 == b._1 && (a._2 - 1 == b._2 || a._2 + 1 == b._2) || // второй символ снизу и слево или справо от первого
      a._1 + 1 == b._1 && (a._2 - 1 == b._2 || a._2 + 1 == b._2) // второй сисвол сверху и слево или справо от первого

  private def isDirect(a: (Int, Int), b: (Int, Int)): Boolean =
    a._1 == b._1 && (a._2 - 1 == b._2 || a._2 + 1 == b._2) || // второй символ выше или ниже и на той же вертикали
      a._2 == b._2 && (a._1 - 1 == b._1 || a._1 + 1 == b._1) // второй символ правее или левее и на той же горизонтали

  private val coordinates: Map[Char, (Int, Int)] = Map(
    'я' -> (0, 0),
    'ч' -> (0, 1),
    'с' -> (0, 2),
    'м' -> (0, 3),
    'и' -> (0, 4),
    'т' -> (0, 5),
    'ь' -> (0, 6),
    'б' -> (0, 7),
    'ю' -> (0, 8),
    'ф' -> (1, 0),
    'ы' -> (1, 1),
    'в' -> (1, 2),
    'а' -> (1, 3),
    'п' -> (1, 4),
    'р' -> (1, 5),
    'о' -> (1, 6),
    'л' -> (1, 7),
    'д' -> (1, 8),
    'ж' -> (1, 9),
    'э' -> (1, 10),
    //  'ё'->(0,1),
    'й' -> (2, 0),
    'ц' -> (2, 1),
    'у' -> (2, 2),
    'к' -> (2, 3),
    'е' -> (2, 4),
    'н' -> (2, 5),
    'г' -> (2, 6),
    'ш' -> (2, 7),
    'щ' -> (2, 8),
    'з' -> (2, 9),
    'х' -> (2, 10),
    'ъ' -> (2, 11)
  ).withDefaultValue((-1, -1))
}