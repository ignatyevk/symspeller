package symspeller.utils

import _root_.petrovich.data.PersonPart
import petrovich._

import scala.collection.immutable
import scala.util.{Failure, Success, Try}

// служебный класс для генерации имен во всех падежах
object Petrovich {
  val allCases = List(Case.Accusative, Case.Dative, Case.Genitive, Case.Instrumental, Case.Nominative, Case.Prepositional)

  def person(s1: String, s2: String, s3: String): List[PersonPart] =
    LastName(s1) ::
      FirstName(s2) ::
      MiddleName(s3)

  def result(person: List[PersonPart], gen: Case): immutable.Seq[String] = {
    val r = petrovich(person, gen)
    List(r.lastFirstMiddle)
  }

  def getAll(s1: String, s2: Option[String], s3: Option[String]): Set[String] = {
    allCases.flatMap(gen => {
        if (s3.nonEmpty&&s2.nonEmpty) result(person(s1, s2.get, s3.get), gen)
        else {
          Utility.write(s1 + " " + s2.getOrElse("???" + " " + s3.getOrElse("???")),"src/main/resources/fuckNames")
          List()
        }
    }).toSet
  }
}
