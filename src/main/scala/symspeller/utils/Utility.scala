package symspeller.utils

import java.io._

import cats.effect.IO
import cats.implicits._
import com.univocity.parsers.csv.{CsvParser, CsvParserSettings}
import scalacache.redis.RedisCache
import scalacache.{Cache, Mode}
import symspeller.interpreter.SpellingErrors

import scala.collection.JavaConverters._
import scala.collection.convert.ImplicitConversions.`collection AsScalaIterable`
import scala.collection.mutable
import scala.io.BufferedSource

object Utility {
  private val settings = new CsvParserSettings()
  settings.setMaxCharsPerColumn(8194)
  settings.setHeaderExtractionEnabled(true)

  def isFileEmpty(path: String): Boolean = {
    val file = new File(path)
    !file.exists || (file.length == 0)
  }

  def distance: String => Int = (token: String) => if (token.length < 7) 1 else if (token.length > 15) 3 else 2

  /**
   * reads unigramms and bigramms
   *
   * @param source from file
   * @return associated ngram with frequency
   */
  def readCsv(source: BufferedSource): Map[String, String] =
    new CsvParser(settings).
      iterate(source.bufferedReader()).asScala.
      map(x => x(1) -> x(2)).toMap

  def readTerms(source: BufferedSource, averageFreq: Int): Map[String, String] =
    source.
      mkString.
      split("\n").
      map(name => name.toLowerCase -> averageFreq.toString).
      toMap

  def readTermsForSegmentor(source: BufferedSource, averageFreq: Int): Map[String, String] =
    source.
      mkString.
      split("\n").flatMap(_.split(" ")).
      map(name => name.toLowerCase -> averageFreq.toString).
      toMap

  //  private def writeNames(source: BufferedSource, filename: String) = {
  //    val l1 = StringBuilder.newBuilder
  //    val l2 = StringBuilder.newBuilder
  //    val qq: Array[String] = source.mkString.split("\n")
  //    qq.foreach(x => {
  //      val t = x.split(" ")
  //      if (t.length == 3) {
  //        val p: Set[String] = Petrovich.getAll(t(0), Some(t(1)), Some(t(2)))
  //        p.foreach(x => {
  //          val y = l1 ++= x ++ " "
  //          y
  //        })
  //      }
  //      else {
  //        l2 ++= (x) ++ "\n"
  //      }
  //    })
  //  }

  def write(s: String, path: String): Unit = {
    val bw = new BufferedWriter(new FileWriter(new File(path)))
    bw.write(s)
    bw.close()
  }

  /**
   * reads cities
   *
   * @param averageFreq average of frequency of all unigrams
   * @return
   */
  def readCities(source: BufferedSource, averageFreq: Int): Map[String, String] =
    new CsvParser(settings).
      iterate(source.bufferedReader()).
      asScala.
      flatMap(_.toList).
      map(city => city.toLowerCase -> averageFreq.toString).
      toMap


  def readWithAverageFreq(source: BufferedSource, averageFreq: Int): Map[String, String] =
    source.
      mkString.
      split("\n").flatMap(x => x.split(" ")).
      map(name => name.toLowerCase -> averageFreq.toString).
      toMap

  def readTerms(source: BufferedSource): Set[String] =
    source.
      mkString.
      split("\n").toSet

  def readAbbreviations(source: BufferedSource): Set[String] =
    source.mkString.split("\n").map(_.split("\t")(0).toLowerCase).toSet

  def readAbbr(source: BufferedSource): Map[String, String] =
    source.mkString.split("\n").map(x => {
      val tt = x.split("\t")
      tt(0).toLowerCase -> tt(1).toLowerCase
    }).toMap

  def averageFrequencyUnigram(unigram: Map[String, String]): Int = if (unigram.isEmpty) 0 else unigram.values.map(_.toInt).sum / unigram.size

  def readDictionary(source: BufferedSource): Map[String, String] =
    source.
      getLines.
      filterNot(_.trim.isEmpty).
      flatMap(row => {
        val goodAndVariants: Array[String] = row.split(":")
        if (goodAndVariants.length != 2) Array(("", ""))
        else
          goodAndVariants(1).split(" ").map((_, goodAndVariants(0)))
      }).toMap

  def dictToRedis(bufferedSource: IO[BufferedSource], cache: Cache[String])(implicit mode: Mode[IO]): IO[Unit] =
    bufferedSource.map(x => x.
      getLines.
      foreach(m2 => {
        val goodAndVariants: Array[String] = m2.split(":")
        if (goodAndVariants.length == 2) {
          goodAndVariants(1).split(" ").foreach({ x =>
            //            cache.remove(x + ":dict").unsafeRunSync()
            //            println(x)
            cache.put(x.toLowerCase + ":dict")(goodAndVariants(0).toLowerCase).unsafeRunSync()
          })
        }
      }))


  def readDictionaryWriteToRedisTermsq(bufferedSource: BufferedSource, cache: Cache[String], p1: String)(implicit mode: Mode[IO]): IO[Unit] = {
    val set = new mutable.HashSet[String]()
    bufferedSource.getLines.
      foreach(x => {
        x.split(" ").map(xx => set += xx)
      }
      )
    //db0:keys=14105821,expires=0,avg_ttl=0
    //127.0.0.1:6379>

    //db0:keys=14099325,expires=0,avg_ttl=0
    val builder = StringBuilder.newBuilder

    val tt: mutable.Set[(String, Set[String])] = set.map(x => {
      x -> SpellingErrors.generateDeletions(x, distance(x))
    })

    val pw = new PrintWriter(p1) {
      tt.map(x => {
        builder.append(x._1)
        builder.append(":")
        x._2.foreach { xy =>
          builder.append(xy)
          builder.append(" ")
        }
        write(builder.toString().trim)
        write("\n")
        builder.clear()
      })
      close()

    }
    pw.close()
    IO(Unit)
  }


  def readDictionaryWriteToRedisTermsw(bufferedSource: BufferedSource, cache: Cache[String], p1: String)(implicit mode: Mode[IO]): IO[Unit] = {
    val set = new mutable.HashSet[String]()
    bufferedSource.getLines.
      foreach(x => {
        val ttt = x.split(",").toSet
        ttt.map(xx => set += xx)
      })
    val builder = StringBuilder.newBuilder

    val pw = new PrintWriter(p1) {
      set.map(x => {
        builder.append(x)
        write(builder.toString().trim)
        write("\n")
        builder.clear()
      })
      close()
    }
    pw.close()
    IO(Unit)
  }

  def validToRedis(u: Map[String, String], cache: Cache[String], namespace: String)(implicit mode: Mode[IO]): IO[Unit] = {
    var t = 0
    val tt = IO(u.foreach(m2 => {
      if (m2._1.length > 3) cache.put(m2._1.toLowerCase + namespace)(m2._2.toLowerCase).unsafeRunSync()
      //      println(m2._1)
      //      cache.remove(m2._1.toLowerCase + namespace).unsafeRunSync()
    })).unsafeRunSync()
    tt
    IO(Unit)
  }

  //db0:keys=14105968,expires=0,avg_ttl=0
  //db0:keys=13815307,expires=0,avg_ttl=0
  //  db0:keys=13815983,expires=0,avg_ttl=0
  //db0:keys=13814226,expires=0,avg_ttl=0
  //db0:keys=13815946,expires=0,avg_ttl=0
  def splitDictFileToFour(bufferedSource: BufferedSource, path1: String)(implicit mode: Mode[IO]): IO[Unit] = {
    val builder = StringBuilder.newBuilder
    IO(println("writing")) *> IO {
      val pw = new PrintWriter(path1) {
        //        val s1 =91959
        //        val s2 =91960-183918
        //        val s3 =183919-275877
        //        val s4 =275877 - 367836
        bufferedSource.getLines.filterNot(_.trim.isEmpty).slice(275877, 367836).
          foreach(m2 => {
            builder.append(m2)
            write(builder.toString().trim)
            write("\n")
            builder.clear()
          })
        close()
      }
      pw.close()
    }
  }

  def dictToString(dictionary: Map[String, String], path: String): Unit = {
    val grouped: Map[String, Iterable[String]] = dictionary.groupBy(_._2).map(x => (x._1, x._2.keys))
    val builder = StringBuilder.newBuilder
    val pw = new PrintWriter(path) {
      grouped.foreach {
        case (k, v) =>
          builder.append(k)
          builder.append(":")
          v.foreach { x =>
            builder.append(x)
            builder.append(" ")
          }
          write(builder.toString().trim)
          write("\n")
          builder.clear()
      }
      close()
    }
    pw.close()
  }

  def smember(key: String, member: String)(implicit mode: Mode[IO], redis: RedisCache[String]): IO[Boolean] =
    mode.M.delay {
      val jedis = redis.jedisPool.getResource
      try {
        jedis.sismember(key, member)
      } finally {
        jedis.close()
      }
    }

  def smembers(key: String, member: String)(implicit mode: Mode[IO], redis: RedisCache[String]): IO[Set[String]] =
    mode.M.delay {
      val jedis = redis.jedisPool.getResource
      try {
        jedis.smembers(key).toSet
      } finally {
        jedis.close()
      }
    }

  def hget(key1: String, key2: String)(implicit mode: Mode[IO], redis: RedisCache[String]): IO[Option[String]] =
    mode.M.delay {
      val jedis = redis.jedisPool.getResource
      try {
        jedis.hget(key1, key2)
      } finally {
        jedis.close()
      }
    }.map(x => Option(x))

  def hset(key1: String, key2: String, value: String)(implicit mode: Mode[IO], redis: RedisCache[String]): IO[Long] =
    mode.M.delay {
      val jedis = redis.jedisPool.getResource
      try {
        jedis.hset(key1, key2, value)
      } finally {
        jedis.close()
      }
    }

  def hmget(key1: String, keys: Seq[String])(implicit mode: Mode[IO], redis: RedisCache[String]): IO[List[String]] =
    mode.M.delay {
      val jedis = redis.jedisPool.getResource
      try {
        jedis.hmget(key1, keys: _*).map(Option(_)).toList.sequence.getOrElse(Nil)
      } finally {
        jedis.close()
      }
    }
}
