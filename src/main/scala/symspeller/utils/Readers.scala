package symspeller.utils

import java.nio.file.{Files, NoSuchFileException, Paths}

import cats.effect.IO

import scala.io.{BufferedSource, Source}

object Readers {
  type Rows = Iterable[List[String]]

  def getReader(filePath: String): IO[BufferedSource] = IO.fromEither({
    if (Files.exists(Paths.get(filePath))) Right(Source.fromFile(filePath, "utf-8"))
    else Left(new NoSuchFileException(filePath))
  })

  def getReader(filename: String, enc: String): IO[BufferedSource] = IO(Source.fromFile(filename, enc))

}