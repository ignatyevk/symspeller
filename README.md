# Speller

- Damerau–Levenshtein distance
- Symmetric speller algorithm instead Norvig (https://towardsdatascience.com/symspellcompound-10ec8f467c9b)
- Scala 2.12, cats, cats-effect, http4s, pureconfig, circe, redis
- Can segmentation (separate by space), correcting text

## usage  

> curl -X POST -d "абронироват переговорю комнатту для константина Игнатева поговорить про ифт на дви" host:5890/transform  

```json  
				{ 
				  "source": "абронироват переговорю комнатту для константина Игнатева поговорить про ифт на дви",
				  "result": "забронировать переговорную комнату для Константина Игнатьева поговорить про ИФТ на ДВИ", 
				  "changes" : {
						"Забронироват" : "забронировать",
						"переговорю" : "переговорную",
						"комнатту" : "комнату",
						"Игнатева" : "игнатьева"
				  },
				   "abbreviations" : [
						"ИФТ",
						"ДВИ"
				  ]
				}
```